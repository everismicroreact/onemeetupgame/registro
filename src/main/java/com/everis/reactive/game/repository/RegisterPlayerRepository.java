package com.everis.reactive.game.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.everis.reactive.game.entitty.orm.Player;

@Repository
public interface RegisterPlayerRepository extends CrudRepository<Player, Integer> {
	@Transactional
	public Player findByIdentification(String identification);
}
