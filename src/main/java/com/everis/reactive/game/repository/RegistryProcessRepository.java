package com.everis.reactive.game.repository;

import org.springframework.data.repository.CrudRepository;

import com.everis.reactive.game.entitty.orm.RegistryProcess;

public interface RegistryProcessRepository extends CrudRepository<RegistryProcess, Integer>{

}
