package com.everis.reactive.game.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.everis.reactive.game.entitty.orm.Vehicle;

@Repository
public interface RegisterVehiclePlayerRepository extends CrudRepository<Vehicle, Integer> {
	@Transactional
	public Vehicle findByNumber(String number);

}
