package com.everis.reactive.game.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.everis.reactive.game.entitty.dto.GamePlayer;
import com.everis.reactive.game.service.RegisterServiceContract;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/game")
public class GameRegisterAPIController {

	@Autowired
	RegisterServiceContract registerService;

	@PostMapping(value = "/register", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@CrossOrigin(origins = "*")
	public ResponseEntity<Object> postRegisterGamePlayer(@RequestBody GamePlayer gamePlayer) {
		try {
			registerService.registerGamePlayer(gamePlayer);
			return ResponseEntity.status(HttpStatus.CREATED).build();
		} catch (RuntimeException exc) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Mono.just(exc.toString()));
		}
	}

	@PutMapping(value = "/register/status", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@CrossOrigin(origins = "*")
	public ResponseEntity<Object> lockRegistry() {
		try {
			registerService.lockRegistryProcess();
			return ResponseEntity.status(HttpStatus.CREATED).build();
		} catch (RuntimeException exc) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Mono.just(exc.toString()));
		}
	}

	@GetMapping(value = "/register/status", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
	@CrossOrigin(origins = "*")
	public Flux<Object> getStatus() {
		return Flux.merge(registerService.loadRegistryProcessStatus(), registerService.notifyChangeRegistryStatus());
	}

}
