package com.everis.reactive.game.entitty.orm;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.everis.reactive.game.entitty.dto.PlayerForm;

@Entity(name = "player")
@Table(name = "player")
@Access(AccessType.PROPERTY)
public class Player extends PlayerForm {
	private int idPlayer;

	@Id
	@Column(name = "id_player")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getIdPlayer() {
		return idPlayer;
	}

	@Id
	@Column(name = "id_player")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public void setIdPlayer(int idPlayer) {
		this.idPlayer = idPlayer;
	}

	@Override
	@Column(name = "user")
	public String getUser() {
		return super.getUser();
	}

	@Override
	@Column(name = "user")
	public void setUser(String user) {
		super.setUser(user);
	}

	@Override
	@Column(name = "identification")
	public String getIdentification() {
		return super.getIdentification();
	}

	@Override
	@Column(name = "identification")
	public void setIdentification(String identification) {
		super.setIdentification(identification);
	}

}