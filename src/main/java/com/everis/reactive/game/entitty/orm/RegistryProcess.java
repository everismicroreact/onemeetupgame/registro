package com.everis.reactive.game.entitty.orm;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "registry")
@Table(name = "registry")
@Access(AccessType.PROPERTY)
public class RegistryProcess {
	private int idRegistry;
	private boolean enabled;

	@Id
	@Column(name = "id_registry")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getIdRegistry() {
		return idRegistry;
	}

	@Id
	@Column(name = "id_registry")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public void setIdRegistry(int idRegistry) {
		this.idRegistry = idRegistry;
	}

	@Column(name = "status")
	public boolean isEnabled() {
		return enabled;
	}

	@Column(name = "status")
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}