package com.everis.reactive.game.entitty.dto;

public class GamePlayer {
	
	private PlayerForm player;
	private VehicleForm vehicle;
	public PlayerForm getPlayer() {
		return player;
	}
	public void setPlayer(PlayerForm player) {
		this.player = player;
	}
	public VehicleForm getVehicle() {
		return vehicle;
	}
	public void setVehicle(VehicleForm vehicle) {
		this.vehicle = vehicle;
	}

}
