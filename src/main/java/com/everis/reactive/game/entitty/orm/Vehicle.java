package com.everis.reactive.game.entitty.orm;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.everis.reactive.game.entitty.dto.VehicleForm;

@Entity(name = "vehicle")
@Table(name = "vehicle")
@Access(AccessType.PROPERTY)
public class Vehicle extends VehicleForm {
	private int idVehicle;

	private Player player;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_vehicle")
	public int getIdVehicle() {
		return idVehicle;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_vehicle")
	public void setIdVehicle(int idVehicle) {
		this.idVehicle = idVehicle;
	}

	@ManyToOne
	@JoinColumn
	public Player getPlayer() {
		return player;
	}

	@ManyToOne
	@JoinColumn()
	public void setPlayer(Player player) {
		this.player = player;
	}
	
	@Override
	@Column(name = "color")
	public String getColor() {
		return super.getColor();
	}

	@Override
	@Column(name = "color")
	public void setColor(String color) {
		super.setColor(color);
	}

	@Override
	@Column(name = "number")
	public String getNumber() {
		return super.getNumber();
	}

	@Override
	@Column(name = "number")
	public void setNumber(String number) {
		super.setNumber(number);
	}


}
