package com.everis.reactive.game.entitty.dto;

public class VehicleForm {
	private String color;
	private String number;

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

}
