package com.everis.reactive.game.service.exceptions;

public class VehicleAlreadyInUseException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public VehicleAlreadyInUseException() {
		super();
	}

	public VehicleAlreadyInUseException(String message) {
		super(message);
	}

	public VehicleAlreadyInUseException(Throwable cause) {
		super(cause);
	}

	public VehicleAlreadyInUseException(String message, Throwable cause) {
		super(message, cause);
	}

	public VehicleAlreadyInUseException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
