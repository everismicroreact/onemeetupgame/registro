package com.everis.reactive.game.service;

import com.everis.reactive.game.entitty.dto.GamePlayer;

public interface RegistryNotificationContract {

	public void notifyGamePlayerRegistry(GamePlayer gamePlayer);
	
	
}
