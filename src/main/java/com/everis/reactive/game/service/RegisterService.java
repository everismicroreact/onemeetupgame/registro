package com.everis.reactive.game.service;

import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.everis.reactive.game.entitty.dto.GamePlayer;
import com.everis.reactive.game.entitty.dto.PlayerForm;
import com.everis.reactive.game.entitty.dto.VehicleForm;
import com.everis.reactive.game.entitty.orm.Player;
import com.everis.reactive.game.entitty.orm.RegistryProcess;
import com.everis.reactive.game.entitty.orm.Vehicle;
import com.everis.reactive.game.repository.RegisterPlayerRepository;
import com.everis.reactive.game.repository.RegisterVehiclePlayerRepository;
import com.everis.reactive.game.repository.RegistryProcessRepository;
import com.everis.reactive.game.service.exceptions.PlayerAlreadyExistsException;
import com.everis.reactive.game.service.exceptions.VehicleAlreadyInUseException;

import reactor.core.publisher.EmitterProcessor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class RegisterService implements RegisterServiceContract, RegistryNotificationContract {

	@Autowired
	RegisterPlayerRepository registerPlayerRepository;

	@Autowired
	RegisterVehiclePlayerRepository registerVehicleRepository;

	@Autowired
	RegistryProcessRepository processRepository;

	@Autowired
	private Source source;

	private final EmitterProcessor<ServerSentEvent<Boolean>> emitterRegisterStatus = EmitterProcessor
			.create(false);

	@Override
	public void registerGamePlayer(GamePlayer gamePlayer) {
		persistGamePlayer(gamePlayer);
		notifyGamePlayerRegistry(gamePlayer);
	}

	@Transactional
	private void persistGamePlayer(GamePlayer gamePlayer) {
		Player playerFound = registerPlayerRepository.findByIdentification(gamePlayer.getPlayer().getIdentification());
		Vehicle vehicleFound = registerVehicleRepository.findByNumber(gamePlayer.getVehicle().getNumber());

		if (playerFound != null) {
			throw new PlayerAlreadyExistsException("La identificacion del usuario ya se encuentra en uso");
		} else if (vehicleFound != null) {
			throw new VehicleAlreadyInUseException("El numero de vehiculo ya se encuentra asignado");
		}

		// transformacion
		Player playerORM = this.adaptGamePlayerDTOToPlayerORM(gamePlayer.getPlayer());
		Vehicle vehicleORM = this.adaptGameVehicleDTOToVehicleORM(gamePlayer.getVehicle());
		vehicleORM.setPlayer(registerPlayerRepository.save(playerORM));
		registerVehicleRepository.save(vehicleORM);

	}

	@Override
	@SendTo(Source.OUTPUT)
	public void notifyGamePlayerRegistry(GamePlayer player) {
		source.output().send(MessageBuilder.withPayload(player).setHeader("operation", "registry").build());

	}

	private Player adaptGamePlayerDTOToPlayerORM(PlayerForm playerForm) {
		Player playerORM = new Player();
		playerORM.setIdentification(playerForm.getIdentification());
		playerORM.setUser(playerForm.getUser());
		return playerORM;
	}

	private Vehicle adaptGameVehicleDTOToVehicleORM(VehicleForm vehicleForm) {
		Vehicle vehicleORM = new Vehicle();
		vehicleORM.setColor(vehicleForm.getColor());
		vehicleORM.setNumber(vehicleForm.getNumber());
		return vehicleORM;
	}

	@Override
	public void lockRegistryProcess() {
		Iterable<RegistryProcess> itRegistry = processRepository.findAll();
		StreamSupport.stream(itRegistry.spliterator(), false).map(x -> {
			x.setEnabled(!x.isEnabled());
			return x;
		}).map(processRepository::save).map(x -> {
			emitterRegisterStatus
					.onNext(ServerSentEvent.builder(x.isEnabled()).id(UUID.randomUUID().toString()).build());
						
			return x;
		}).collect(Collectors.toList());

	}

	@Override
	public Mono<Boolean> loadRegistryProcessStatus() {
		return Mono.just(cargarStatus());

	}

	private Boolean cargarStatus() {
		RegistryProcess registryProcess = processRepository.findAll().iterator().next();

		return Boolean.valueOf(registryProcess.isEnabled());
	}

	@Override
	public Flux<ServerSentEvent<Boolean>> notifyChangeRegistryStatus() {
		return emitterRegisterStatus.log();
	}

}
