package com.everis.reactive.game.service.exceptions;

public class PlayerAlreadyExistsException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PlayerAlreadyExistsException() {
		super();
	}

	public PlayerAlreadyExistsException(String message) {
		super(message);
	}

	public PlayerAlreadyExistsException(Throwable cause) {
		super(cause);
	}

	public PlayerAlreadyExistsException(String message, Throwable cause) {
		super(message, cause);
	}

	public PlayerAlreadyExistsException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
