package com.everis.reactive.game.service;

import org.springframework.http.codec.ServerSentEvent;

import com.everis.reactive.game.entitty.dto.GamePlayer;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface RegisterServiceContract {

	public void registerGamePlayer(GamePlayer gamePlayer);

	public void lockRegistryProcess();

	public Flux<ServerSentEvent<Boolean>> notifyChangeRegistryStatus();

	public Mono<Boolean> loadRegistryProcessStatus();
	
}
