Para este microservicio es prerrequisito contar con una base de datos en MySQL y un topico en kafka con retención de 1 segundo.
**Script SQL para la base de datos:**

```
-- Volcando estructura para tabla meetupgame.player
DROP TABLE IF EXISTS `player`;
CREATE TABLE IF NOT EXISTS `player` (
  `id_player` int(11) NOT NULL AUTO_INCREMENT,
  `identification` varchar(255) DEFAULT NULL,
  `user` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_player`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

-- Volcando estructura para tabla meetupgame.registry
DROP TABLE IF EXISTS `registry`;
CREATE TABLE IF NOT EXISTS `registry` (
  `id_registry` int(11) NOT NULL AUTO_INCREMENT,
  `status` binary(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_registry`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla meetupgame.registry: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `registry` DISABLE KEYS */;
REPLACE INTO `registry` (`id_registry`, `status`) VALUES
	(1, _binary 0x31);
/*!40000 ALTER TABLE `registry` ENABLE KEYS */;

-- Volcando estructura para tabla meetupgame.vehicle
DROP TABLE IF EXISTS `vehicle`;
CREATE TABLE IF NOT EXISTS `vehicle` (
  `id_vehicle` int(11) NOT NULL AUTO_INCREMENT,
  `id_player` int(11) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `number` varchar(255) DEFAULT NULL,
  `player_id_player` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_vehicle`),
  KEY `FK_Player` (`id_player`),
  KEY `FKj96nwrnne009qtjsgjjbiadfm` (`player_id_player`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;


/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
```

**Creación de tópico en kafka**

Se requiere un topico cuyo nombre se parametrizara en las variables de entorno. Lo importante es que el periodo de retención sea bajo, por defecto 1 segundo.

`kafka-topics.sh --zookeeper 192.168.0.50:2181 --alter --topic registry --config retention.ms=1000`

**Docker**

La imagen de docker para este microservicio se genera a partir del Siguiente Dockerfile, que hace referencia al paquete jar generado para la última versión en código estable. Durante la ejecución, recibe 8 parametros que asigna a variables de entorno que deben enviarse al momento de levantar el contenedor. En el archivo Dockerfile se incluye valores por defecto que seguramente no funcionarán en su ambiente.

```
FROM openjdk:8u131-jre-alpine
COPY ./registro-0.0.1-SNAPSHOT.jar app.jar

ENV MEETUP_SERVER_PORT 8080
ENV MEETUP_KAFKA_SERVER	192.168.0.50
ENV MEETUP_KAFKA_PORT	9092
ENV MEETUP_KAFKA_TOPIC_REGISTRY	registry
ENV MEETUP_MYSQL_SERVER	192.168.0.50
ENV MEETUP_MYSQL_PORT	3306
ENV MEETUP_MYSQL_DBNAME	meetupgame
ENV MEETUP_MYSQL_USER	gameuser
ENV MEETUP_MYSQL_PASSWORD	"set_pass"
EXPOSE $MEETUP_SERVER_PORT

CMD ["/usr/bin/java", "-jar", "app.jar"]

```

**Build de este Dockerfile**:

`docker build -f Dockerfile -t meetup/registro .`

**Run de este Dockerfile**:

`docker run -d --name registro -e MEETUP_KAFKA_SERVER=[IP KAFKA] -e MEETUP_KAFKA_PORT=[PUERTO KAFKA]-e MEETUP_KAFKA_TOPIC_REGISTRY=registry -e MEETUP_MYSQL_SERVER=[IP SERVER MYSQL] -e MEETUP_MYSQL_PORT=[PUERTO MYSQL] -e MEETUP_MYSQL_DBNAME=[NOMBRE BD MYSQL]-e MEETUP_MYSQL_USER=[USUARIO MYSQL] -e MEETUP_MYSQL_PASSWORD=[PASSWORD USUARIO MYSQL]  -e MEETUP_SERVER_PORT=[PUERTO HTTP PARA EXPONER ESTE MS]  -p 8090:8090 meetup/registro`

**Argumentos**

* MEETUP_KAFKA_SERVER	Ip del servidor de kafka
* MEETUP_KAFKA_PORT	Puerto para el servidor kafka
* MEETUP_KAFKA_TOPIC_REGISTRY	nombre del topico para el registro. por defecto registry
* MEETUP_MYSQL_SERVER	Ip del servidor de BD Mysql
* MEETUP_MYSQL_PORT	Puerto del servidor para mysql
* MEETUP_MYSQL_DBNAME	nombre de la base de datos
* MEETUP_MYSQL_USER	nombre de usuario de la base de datos
* MEETUP_MYSQL_PASSWORD	password para la base de datos y el usuario
* MEETUP_SERVER_PORT Puerto a exponer para el microservicio